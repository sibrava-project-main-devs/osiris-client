FROM nginx

LABEL maintainer="Freem~ <freemanovec@protonmail.com>"
RUN DEBIAN_FRONTEND=noninteractive apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y nodejs curl software-properties-common
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && DEBIAN_FRONTEND=noninteractive apt install -y npm
RUN npm install -g yarn
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/src/app
COPY package*.json ./
RUN yarn install
COPY . .
RUN yarn build

RUN mkdir /app
RUN cp -avr /usr/src/app/dist/* /app/

ENTRYPOINT [ "/entrypoint.sh" ]
