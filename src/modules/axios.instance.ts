import Axios from "axios";

const instance = Axios.create({
  baseURL: process.env.VUE_APP_HOST_API,
  withCredentials: true
});

export default instance;
