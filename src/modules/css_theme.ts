import { ThemeVariable } from "@interfaces/theme_variable.interface";

export class CssTheme {
  public readonly icon: string;
  public readonly cssValues: ThemeVariable;
  constructor(
    icon: string,
    text: string,
    shapes: string,
    accent: string,
    background: string,
    alternativeBackground: string,
    backupBackground: string
  ) {
    this.icon = icon;
    this.cssValues = {
      text,
      shapes,
      accent,
      background,
      background2: background + "33",
      background3: background + "4D",
      background4: background + "66",
      background5: background + "80",
      background6: background + "99",
      background7: background + "B3",
      background8: background + "CC",
      alternativeBackground,
      backupBackground
    };
  }
}
