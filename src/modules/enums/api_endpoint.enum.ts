/* this should get updated as Rohla makes more endpoints */
enum ApiEndpoint {
  loginPassword = "/login/password",
  googleUrl = "/login/google/url",
  googleCid = "/login/google/cid",
  googleCallback = "/login/google/callback",
  githubUrl = "/login/github/url",
  githubCid = "/login/github/cid",
  githubCallback = "/login/github/callback",
  gitlabUrl = "/login/gitlab/url",
  gitlabCid = "/login/gitlab/cid",
  gitlabCallback = "/login/gitlab/callback",
  registration = "/registration",
  registrationValidateUsername = "/registration-validation/username",
  registrationValidatePassword = "/registration-validation/password",
  user = "/user",
  userList = "/user/list",
  email = "/mail",
  folder = "/mail_folder",
  folders = "/mail_folder/list",
  addEmail = "/inbox_connection/add",
  changeUsername = "/user/change_name",
  setSettings = "/user-configuration/set",
  getSettings = "/user-configuration/get",
  sendEmail = "/mail/send"
}

export default ApiEndpoint;
