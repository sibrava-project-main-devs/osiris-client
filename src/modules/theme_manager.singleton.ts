import { CssTheme } from "@modules/css_theme";
import themes from "@assets/styles/themes";
import { Theme } from "@enums/theme.enum";

class ThemeManager {
  private theme: CssTheme;

  constructor(theme: Theme) {
    this.theme = this.checkTheme(theme);
    this.applyTheme();
  }

  public get themeName(): string {
    return this.theme.cssValues.text;
  }

  public get icon(): string {
    return this.theme.icon;
  }

  public updateTheme(theme: Theme) {
    this.theme = this.checkTheme(theme);
    this.applyTheme();
  }

  private applyTheme() {
    for (let property in this.theme.cssValues) {
      if (this.theme.cssValues.hasOwnProperty(property)) {
        document.body.style.setProperty(
          `--${property}`,
          this.theme.cssValues[property]
        );
      }
    }
  }

  private checkTheme(theme: Theme): CssTheme {
    const cssTheme = themes.getValue(theme);

    if (cssTheme !== undefined) {
      return cssTheme;
    } else {
      throw new Error(`Theme '${theme}' is not defined.`);
    }
  }
}

const themeMananger = new ThemeManager(Theme.light);
export default themeMananger;
