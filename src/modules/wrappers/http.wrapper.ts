import Axios from "@modules/axios.instance";
import { AxiosRequestConfig } from "axios";
import ApiEndpoint from "@enums/api_endpoint.enum";

class HttpStatic {
  public async get(
    url: ApiEndpoint,
    config?: AxiosRequestConfig | undefined
  ): Promise<any> {
    return Axios.get(url, config);
  }

  public async post(
    url: ApiEndpoint,
    data?: any,
    config?: AxiosRequestConfig | undefined
  ): Promise<any> {
    return Axios.post(url, data, config);
  }

  public async put(
    url: ApiEndpoint,
    data?: any,
    config?: AxiosRequestConfig | undefined
  ) {
    return Axios.put(url, data, config);
  }

  public async patch(
    url: ApiEndpoint,
    data?: any,
    config?: AxiosRequestConfig | undefined
  ) {
    return Axios.patch(url, data, config);
  }
}

export default new HttpStatic();
