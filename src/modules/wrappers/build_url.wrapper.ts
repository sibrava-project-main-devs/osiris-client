import build from "build-url";

interface UrlOptions {
  path: string;
  queryParams: {};
}

export function buildUrl(url: string, options?: UrlOptions): string {
  return build(url, options);
}
