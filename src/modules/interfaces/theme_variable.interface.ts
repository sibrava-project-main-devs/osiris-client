export interface ThemeVariable {
  [property: string]: string;

  readonly text: string;
  readonly shapes: string;
  readonly accent: string;
  readonly background: string;
  readonly background2: string;
  readonly background3: string;
  readonly background4: string;
  readonly background5: string;
  readonly background6: string;
  readonly background7: string;
  readonly background8: string;
  readonly alternativeBackground: string;
  readonly backupBackground: string;
}
