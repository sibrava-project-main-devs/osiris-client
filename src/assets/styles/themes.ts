import { CssTheme } from "@modules/css_theme";
import { Dictionary } from "typescript-collections";
import { Theme } from "@enums/theme.enum";

const themes: Dictionary<Theme, CssTheme> = new Dictionary<Theme, CssTheme>();

// light theme
themes.setValue(
  Theme.light,
  new CssTheme(
    "fas fa-sun",
    "#161618",
    "#808096",
    "#242426",
    "#F0F0FF",
    "#B8B8C8",
    "#A0A0AA"
  )
);

// dark theme
themes.setValue(
  Theme.dark,
  new CssTheme(
    "fas fa-moon",
    "#F0F0FF",
    "#B8B8C8",
    "#A0A0AA",
    "#242426",
    "#808096",
    "#161618"
  )
);

export default themes;
