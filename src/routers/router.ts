import Vue from "vue";
import VueRouter from "vue-router";
import ClientPage from "@views/client/ClientPage.vue";
import HomePage from "@views//homepage/HomePage.vue";
import LoginPage from "@views/authentication/login/LoginPage.vue";
import RegistrationPage from "@views/authentication/registration/RegistrationPage.vue";
import GithubLoginCallbackPage from "@views/authentication/login/callback/GithubLoginCallbackPage.vue";
import GitlabLoginCallbackPage from "@views/authentication/login/callback/GitlabLoginCallbackPage.vue";
import GoogleLoginCallbackPage from "@views/authentication/login/callback/GoogleLoginCallbackPage.vue";
import NotFoundPage from "@views/not_found/NotFoundPage.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage
    },
    {
      path: "/login",
      name: "login",
      component: LoginPage
    },
    {
      path: "/registration",
      name: "registration",
      component: RegistrationPage
    },
    {
      path: "/client",
      name: "client",
      component: ClientPage
    },
    {
      path: "/login/github/callback",
      name: "github",
      component: GithubLoginCallbackPage
    },
    {
      path: "/login/gitlab/callback",
      name: "gitlab",
      component: GitlabLoginCallbackPage
    },
    {
      path: "/login/google/callback",
      name: "google",
      component: GoogleLoginCallbackPage
    },
    // has to be last!
    {
      path: "*",
      name: "not found",
      component: NotFoundPage
    }
  ]
});
