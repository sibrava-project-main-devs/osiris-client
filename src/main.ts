import Vue from "vue";
import App from "@components/App.vue";
import router from "@routers/router";
import store from "@stores/store";
import VueCookies from "vue-cookies";

Vue.config.productionTip = false;
Vue.use(VueCookies);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
