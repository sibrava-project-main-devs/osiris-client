## Development guide
1. Clone the repo from GitLab
2. Add `.env` file based on `.env.example`
3. Start up the app with `yarn start`

[Configuration](/configuration) - How to set up `.env` file