describe("Simple Redirecting", () => {
  it("homepage successfully loads", () => {
    cy.visit("/");
  });
  it("has title 'osiris'", () => {
    cy.title().should("equal", "osiris");
  });
  it("redirects to '/registration'", () => {
    cy.visit("/registration");
  });
  it("redirects to '/login'", () => {
    cy.visit("/login");
  });
  it("redirects to homepage when logo is clicked", () => {
    cy.get("[data-cy=logo-click]").click();
    cy.location().should(location => {
      expect(location.pathname).to.equal("/");
    });
  });
  it("redirects to 404 page on unrecognised path", () => {
    cy.visit("/oof");
    cy.contains("404 Not found!");
  });
});
