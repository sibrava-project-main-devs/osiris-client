describe("Login with mocked endpoint", () => {
  it("homepage successfully loads", () => {
    cy.visit("/");
  });
  it("clicks login button to redirect to '/login'", () => {
    cy.get("[data-cy=login]").click();
    cy.location().should(location => {
      expect(location.pathname).to.equal("/login");
    });
  });
  it("contains 'username' and 'password' text boxes", () => {
    cy.get("[data-cy=username]").type("Noobmaster69");
    cy.get("[data-cy=password]").type("asdf1234");
  });
  it("redirects to client page when login button is clicked", () => {
    cy.server();
    cy.route("POST", "/api/login/password", { state: "ok" });
    cy.setCookie("Token", "OwO");

    cy.get("[data-cy=login-button]").click();
    cy.location().should(location => {
      expect(location.pathname).to.equal("/client");
    });
  });
  it("redirects to home page when sign out button is clicked and removes the cookie", () => {
    cy.setCookie("Token", "OwO");

    cy.get("[data-cy=logout-button]").click();
    cy.location().should(location => {
      expect(location.pathname).to.equal("/");
    });

    cy.getCookie("Token").should("not.exist");
  });
});
