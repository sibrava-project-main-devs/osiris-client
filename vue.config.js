const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = {
  configureWebpack: {
    devtool: "source-map",
    resolve: {
      plugins: [new TsconfigPathsPlugin({})]
    }
  }
};
