#!/usr/bin/env bash

cd /usr/src/app;

echo "Building the project";
rm -rf dist/ || echo "Fine";
rm -rf /app/* || echo "Fine"
yarn build;
echo "Copying files over";
cp -avr dist/* /app/;
cd /app;
echo "Starting nginx";
nginx -g "daemon off";
echo "Stopping";