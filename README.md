## Development guide
1. Clone the repo from GitLab
2. Add `.env` file based on `.env.example`
3. Start up the app with `yarn start`

For info on how to set up `.env`, see https://sibrava-project-main-devs.gitlab.io/osiris-client/#/configuration

### Documentation
Documentation can be found on: https://sibrava-project-main-devs.gitlab.io/osiris-client